extends Node

enum ACTIONS{
	SYNC,
	INIT,
	REGISTER,
	LOGIN,
	BUPGRADE,
	BREAK_BUPGRADE,
	END_BUPGRADE,
	SHORTEN_BUPGRADE,
	TUPGRADE,
	BREAK_TUPGRADE,
	END_TUPGRADE,
	SHORTEN_TUPGRADE,
	GET_AVATAR
}

const PRINT_ACITON: bool = false
const API_HOST_URL: String = ""
const API_HEADERS: PoolStringArray = PoolStringArray(["Content-Type: application/json"])
const CONNECTION_WAITER: PackedScene = preload("res://Scenes/SingletonsScenes/ConnectingWaiter/ConnectingWaiter.tscn")

var _api_urls: Dictionary = {
	ACTIONS.SYNC: API_HOST_URL + "/api/sync",
	ACTIONS.INIT: API_HOST_URL + "/api/init",
	ACTIONS.BUPGRADE: API_HOST_URL + "/api/bu",
	ACTIONS.BREAK_BUPGRADE: API_HOST_URL + "/api/bb",
	ACTIONS.END_BUPGRADE: API_HOST_URL + "/api/bf",
	ACTIONS.SHORTEN_BUPGRADE: API_HOST_URL + "/api/br",
	ACTIONS.REGISTER: API_HOST_URL + "/api/ur",
	ACTIONS.LOGIN: API_HOST_URL + "/api/ul",
	ACTIONS.GET_AVATAR: API_HOST_URL + "/api/uga",
	ACTIONS.TUPGRADE: API_HOST_URL + "/api/tu",
	ACTIONS.END_TUPGRADE: API_HOST_URL + "/api/tf",
	ACTIONS.BREAK_TUPGRADE: API_HOST_URL + "/api/tb",
	ACTIONS.SHORTEN_TUPGRADE: API_HOST_URL + "/api/tr",
}
var _actions_with_connection_waiter: Array = [ACTIONS.LOGIN, ACTIONS.REGISTER]

var _is_api_blocked: bool = false

onready var _http_req: HTTPRequest = _create_http_request()


##Function is resposible for sending request to server and receiving response form it
## takes 'to_action' as value from ACTIONS enum and request body as Dictionary
func get_response_res(to_action: int, body: Dictionary) -> ServerResponse:
	while _is_api_blocked:
		yield(get_tree(), "idle_frame")
	
	var connection_waiter: ConnectionWaiter
	
	_is_api_blocked = true
	
	if PRINT_ACITON:
		print("Performing action: ", ACTIONS.keys()[to_action], " with body: ", body)
	
	if _actions_with_connection_waiter.has(to_action):
		connection_waiter = CONNECTION_WAITER.instance() as ConnectionWaiter
		add_child(connection_waiter)
	
	var result: Array = yield(_post_request(to_action, JSON.print(body)), "completed")
	result.push_front(to_action)
	
	if is_instance_valid(connection_waiter):
		connection_waiter.delete()
	
	_is_api_blocked = false
	
	return ServerResponse.new(result)


func is_api_blocked() -> bool:
	return _is_api_blocked


func _post_request(action: int, body: String) -> Array:
	var err: int = _http_req.request(_api_urls[action], API_HEADERS, false, HTTPClient.METHOD_POST, body)
	
	if not err == OK:
		add_child(ErrorDialogScene.instance(tr("ERR_API_FAILED")))
		push_error("ApiController -> post_request(), something went wrong, error: " + err as String)
		yield(get_tree().create_timer(0.05), "timeout")
		return [FAILED]
	
	return yield(_http_req, "request_completed")


func _create_http_request() -> HTTPRequest:
	var req: HTTPRequest = HTTPRequest.new()
	req.use_threads = false
	add_child(req)
	return req