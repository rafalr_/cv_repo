extends Node

signal synchronization(is_success)

const AUTO_TICKING: bool = false
const DEF_SYNC_PATH: String = "res://default_sync.json"

var uId: String = "" setget set_uId
var sync_data: SyncData = null setget set_sync_data, get_sync_data
var default_sync: SyncData = null setget set_default_sync, get_default_sync
var api_blocked: bool = false

onready var tick_timer: Timer


func _ready() -> void:
	tick_timer = Timer.new()
	tick_timer.wait_time = 1.0
# warning-ignore:return_value_discarded
	tick_timer.connect("timeout", self, "_on_TickTimer_timeout")
	add_child(tick_timer)
	
	_load_default_sync()

##Function resposible for logging user. Takes login and password as Strings.
## If login succeded returns true and sets uId, otherwise returns false
func login(mail: String, password: String) -> bool:
	if ApiController.is_api_blocked():
		yield(get_tree(), "idle_frame")
		return false
	
	var response: ServerResponse = yield(
			ApiController.get_response_res(ApiController.ACTIONS.LOGIN,
			{"email": mail, "password": password}), "completed"
		)
	
	if not response.is_valid():
		return false
	
	if not response.response_type == ServerResponse.TYPE.LOGIN:
		push_error("GameData -> login(), response from server is not login data (" + ServerResponse.TYPE.keys()[response.response_type] + ")")
		return false
	
	uId = response.login_data.token
	
	return true

##Similar to login function, creates user and logs him
func register(_nick: String, mail: String, password: String) -> bool:
	if ApiController.is_api_blocked():
		yield(get_tree().create_timer(0.05), "timeout")
		return false
	
	var response: ServerResponse = yield(
			ApiController.get_response_res(ApiController.ACTIONS.REGISTER,
			{"email": mail, "password": password}), "completed"
		)
	
	if not response.is_valid():
		return false
	
	if not response.response_type == ServerResponse.TYPE.LOGIN:
		push_error("GameData -> register(), response from server is not login data (" + ServerResponse.TYPE.keys()[response.response_type] + ")")
		return false
	
	print(response.login_data)
	
	uId = response.login_data.token
	
	return true

##Function resposible for synchronizing game with server.
## 'final' measn if signal synchronized(false) will be emmited in case of failure (fallback system).
## 'given' is where you can pass ServerResponse if it's already received and only needs to be parsed
func server_sync(final: bool = false, given: ServerResponse = null) -> void:
	print("Synchronizing...")
	
	tick_timer.stop()
	
	if uId == "":
		push_error("GameData -> server_sync(), cannot sync because uId is not set")
		return
	
	var response: ServerResponse
	
	if given == null:
		response = yield(
				ApiController.get_response_res(ApiController.ACTIONS.SYNC,
				{"uId": uId}), "completed"
			)
	else:
		response = given
	
	if not response.is_valid():
		Fallback.failed()
		if final:
			emit_signal("synchronization", false)
		print("#Error with server response")
		return
	
	if not response.response_type == ServerResponse.TYPE.SYNC:
		push_error("GameData -> server_sync(), received response of wrong type. Expected SYNC, received: " + response.response_type as String)
		return
	
	sync_data = response.sync_data
	
	Fallback.success()
	
# warning-ignore:return_value_discarded
	sync_data.connect("need_sync", self, "_on_SyncData_need_sync")
	
	tick_timer.start(0.0)
	
	print("#Is sync valid: ", sync_data.is_valid())
	print("#Done")
	emit_signal("synchronization", true)

##Checks if sync_data isn't broken. If it is, function returns default sync (witch default values)
## Warning: it does not checks if Sync exists, if sync_data is null function will also return null
func get_sync_data() -> SyncData:
	if sync_data == null:
		return null
	
	if not sync_data == null and not sync_data.is_valid():
		push_error("GameData -> get_sync_data(), sync_data is broken")
		return SyncData.new(default_sync.json_data.duplicate())
	
	var copy: SyncData = SyncData.new(sync_data.json_data.duplicate())
	
	for i in sync_data._second_count:
		copy.push_second_passed()
	
	return copy

##Returns true when sync_data exists and isn't broken
func is_sync_avaliable() -> bool:
	return not sync_data == null and sync_data.is_valid()

##Looks for passed 'target_name' in sync_data, and checks if requirements are met
func is_upgrade_viable(target_name: String) -> bool:
	var data: SyncData = get_sync_data()
	
	if data == null:
		push_warning("GameData -> is_upgrade_viable(), sync data unavaliable, returning false")
		return false
	
	var upgrade_data: UpgradeData = null
	
	if data.array_of_buildings.has(target_name):
		upgrade_data = (data.array_of_buildings[target_name] as BuildingData).upgrade
	if data.array_of_technologies.has(target_name):
		upgrade_data = (data.array_of_technologies[target_name] as TechnologyData).upgrade
	
	if upgrade_data == null:
		push_error("GameData -> is_upgrade_viable(), target_name not found in sync_data")
		return false
	
	for res_id in upgrade_data.req_res:
		if not Global.RESOURCE.values().has(res_id):
			push_warning("GameData -> is_upgrade_viable(), unrecognized res_id")
			return false
		else:
			if upgrade_data.req_res[res_id] is float or upgrade_data.req_res[res_id] is int:
				if data.resources[res_id] < upgrade_data.req_res[res_id]:
					return false
			else:
				push_warning("GameData -> is_upgrade_viable(), value of req_res to compare is not an number")
				return false
	
	for b_ref in upgrade_data.req_buildings:
		var needed_level = upgrade_data.req_buildings[b_ref]
		
		if needed_level is int or needed_level is float:
			needed_level = needed_level as int
		else:
			push_error("GameData -> is_upgrade_viable(), value of req_buildings to compare is not an number")
			return false
		
		if not data.array_of_buildings.has(b_ref):
			push_error("GameData -> is_upgrade_viable(), building reference in req_building not found in sync_data")
			return false
		
		var building: BuildingData = data.array_of_buildings[b_ref] as BuildingData
		
		if building.level < needed_level:
			return false
	
	for tech_ref in upgrade_data.req_tech:
		var needed_level = upgrade_data.req_tech[tech_ref]
		
		if needed_level is int or needed_level is float:
			needed_level = needed_level as int
		else:
			push_error("GameData -> is_upgrade_viable(), value of req_tech to compare is not an number")
			return false
		
		if not data.array_of_technologies.has(tech_ref):
			push_error("GameData -> is_upgrade_viable(), tech reference in req_tech not found in sync_data")
			return false
		
		var tech: TechnologyData = data.array_of_technologies[tech_ref] as TechnologyData
		
		if tech.level < needed_level:
			return false
	
	return true


func _load_default_sync() -> void:
	var file: File = File.new()
	
	if not file.open(DEF_SYNC_PATH, File.READ) == OK:
		push_error("GameData -> _load_default_sync(), failed to open default_sync.json")
		return
	
	var json: JSONParseResult = JSON.parse(file.get_as_text())
	
	file.close()
	
	if not json.error == OK:
		push_error("GameData -> _load_default_sync(), " + json.error_string + " at line " + (json.error_line as String))
		return
	
	if not typeof(json.result) == TYPE_DICTIONARY:
		push_error("GameData -> _load_default_sync(), unexpected result type of " + (typeof(json.result) as String))
		return
	
	default_sync = SyncData.new(json.result)
	
	assert(not default_sync == null and default_sync.is_valid(), "GameData -> _load_default_sync() -> something is wrong with default sync")


func set_uId(_value: String) -> void:
	push_error("GameData -> set_uId(), uId can be read from but not written to")


func set_sync_data(_value: SyncData) -> void:
	push_error("GameData -> set_sync_data(), cannot change this filed outside form this class")


func set_default_sync(_value: SyncData) -> void:
	push_error("GameData -> set_default_sync(), cannot change this filed outside form this class")


func get_default_sync() -> SyncData:
	push_error("GameData -> get_default_sync(), cannot access this field")
	return null


func _on_TickTimer_timeout() -> void:
	if not sync_data.broken:
		sync_data.push_second_passed()


func _on_SyncData_need_sync(type: int, key: String = "") -> void:
	yield(get_tree().create_timer(0.5), "timeout")
	server_sync()
	
	yield(self, "synchronization")
	
	match type:
		SyncData.UPGRADE_TYPES.BUILDING:
			if not sync_data.array_of_buildings.has(key):
				push_error("GameData -> _on_SyncData_need_sync(), can't display notification, building not found")
			else:
				var data: BuildingData = sync_data.array_of_buildings[key]
				
				var pass_name: String = tr(data.building_reference + "-NAME")
				
				print(data.building_reference + " was upgraded")
				
				if is_instance_valid(Global.main_ui):
					Global.main_ui.add_notification(tr("NOTI_BUPGRADE_TITLE"), pass_name + " " + tr("NOTI_BUPGRADE_MES") + " " + (data.level as String))
				else:
					push_warning("GameData -> _on_SyncData_need_sync(), there is no valid reference to MainUI, can't show notification")
		SyncData.UPGRADE_TYPES.REPRODUCTION:
			var stage: int = int(key)
			
			if not ReproductionData.STAGES.values().has(stage) and ReproductionData.STAGES.values().has(stage+1):
				push_error("GameData -> _on_SyncData_need_sync(), unrecognized stage")
				return
			
			var current_stage: String = ReproductionData.stage_id_to_stage_name(stage)
			var next_stage: String = ReproductionData.stage_id_to_stage_name(stage+1)
			
			if is_instance_valid(Global.main_ui):
				Global.main_ui.add_notification(tr("NOTI_REPRO_TITLE"), current_stage + " " + tr("NOTI_REPRO_MES") + " " + next_stage)
			else:
				push_warning("GameData -> _on_SyncData_need_sync(), there is no valid reference to MainUI, can't show notification")
		SyncData.UPGRADE_TYPES.NEW_EGG:
			if is_instance_valid(Global.main_ui):
				Global.main_ui.add_notification(tr("NOTI_NEW_EGG_TITLE"), tr("NOTI_NEW_EGG_MES"))
			else:
				push_warning("GameData -> _on_SyncData_need_sync(), there is no valid reference to MainUI, can't show notification")
		SyncData.UPGRADE_TYPES.TECHNOLOGY:
			if not sync_data.array_of_technologies.has(key):
				push_error("GameData -> _on_SyncData_need_sync(), can't display notification, technology not found")
			else:
				var data: TechnologyData = sync_data.array_of_technologies[key]
				
				var pass_name: String = tr(data.tech_name + "-NAME")
				
				print(data.tech_name + " was upgraded")
				
				if is_instance_valid(Global.main_ui):
					Global.main_ui.add_notification(tr("NOTI_TUPGRADE_TITLE"), pass_name + " " + tr("NOTI_TUPGRADE_MES") + " " + (data.level as String))
				else:
					push_warning("GameData -> _on_SyncData_need_sync(), there is no valid reference to MainUI, can't show notification")
		_:
			push_warning("GameData -> _on_SyncData_need_sync(), unrecognized upgrade type")
