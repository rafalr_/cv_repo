extends Resource
class_name BaseResource

const CLASS: String = "BASE_RESOURCE"

var broken: bool = false


func _display_error(msg: String) -> void:
	Global.add_child(ErrorDialogScene.instance(msg))


func _return_value(value):
	if broken:
		push_error("BaseResource -> _get(), trying to collect data from broken Resource")
		return null
	
	return value


func is_valid() -> bool:
	var check: bool = true
	
	for i in get_property_list():
		if (i as Dictionary).has("name"):
			if i["name"] == "script":
				continue
			
			var obj = get(i["name"])
			
			if is_base_resource(obj) and obj.has_method("is_valid") and not obj.call("is_valid"):
				check = false
	
	return check and not broken


func is_base_resource(what: Object) -> bool:
	return not what == null and "CLASS" in what and what.get("CLASS") as String
