extends BaseResource
class_name ServerResponse

enum TYPE{UNRECOGNIZED, LOGIN, SYNC, AVATAR}

var response_type: int = TYPE.UNRECOGNIZED
var raw_body: Dictionary
var login_data: LoginData = null setget set_login_data
var sync_data: SyncData = null setget set_sync_data
var avatar_data: AvatarData = null setget set_avatar_data

var _server_errors: Dictionary = {
	"B0": "ERR_SER_B0",
	"B1": "ERR_SER_B1",
	"B2": "ERR_SER_B2",
	"U0": "ERR_SER_U0",
	"EE": "ERR_SER_EE",
	"BD": "ERR_SER_BD",
	"RTNG": "ERR_SER_RTNG",
}
var _action_errors: Dictionary = {
	ApiController.ACTIONS.INIT: "ERR_SER_ACT_INIT",
	ApiController.ACTIONS.SYNC: "ERR_SER_ACT_SYNC",
	ApiController.ACTIONS.BUPGRADE: "ERR_SER_ACT_BUPGRADE",
	ApiController.ACTIONS.BREAK_BUPGRADE: "ERR_SER_ACT_BREAK_BU",
	ApiController.ACTIONS.END_BUPGRADE: "ERR_SER_ACT_END_BU",
	ApiController.ACTIONS.SHORTEN_BUPGRADE: "ERR_SER_ACT_SH_BU",
	ApiController.ACTIONS.REGISTER: "ERR_SER_ACT_REG",
	ApiController.ACTIONS.LOGIN: "ERR_SER_ACT_LOG",
	ApiController.ACTIONS.GET_AVATAR: "ERR_SER_ACT_GETAV",
	ApiController.ACTIONS.TUPGRADE: "ERR_SER_ACT_TUPGRADE",
	ApiController.ACTIONS.BREAK_TUPGRADE: "ERR_SER_ACT_BREAK_TU",
	ApiController.ACTIONS.END_TUPGRADE: "ERR_SER_ACT_END_TU",
	ApiController.ACTIONS.SHORTEN_TUPGRADE: "ERR_SER_ACT_SH_TU",
}
var _request_errors: Dictionary = {
	0: "ERR_SER_REQ_0",
	1: "ERR_SER_REQ_1",
	2: "ERR_SER_REQ_2",
	3: "ERR_SER_REQ_3",
	4: "ERR_SER_REQ_4",
	5: "ERR_SER_REQ_5",
	6: "ERR_SER_REQ_6",
	7: "ERR_SER_REQ_7",
	8: "ERR_SER_REQ_8",
	9: "ERR_SER_REQ_9",
	10: "ERR_SER_REQ_10",
	11: "ERR_SER_REQ_11",
	12: "ERR_SER_REQ_12",
}


func _init(data: Array) -> void:
	if not _is_next_type_correct(TYPE_INT, data[0]):
		return
	
	var action: int = data.pop_front()
	
	if data[0] == FAILED:
		broken = true
		return
	
	if not _check_result_code(data.pop_front()):
		return
	
	if not _is_next_type_correct(TYPE_INT, data[0]):
		return
	
	if not data[0] == HTTPClient.RESPONSE_OK and not data[0] == HTTPClient.RESPONSE_BAD_REQUEST:
		push_error("ServerResponse -> _init(), response failed. code: " + data[0] as String)
		_display_error((tr(_action_errors[action]) if _action_errors.has(action) else tr("ERR_SER_UNRECOGNIZED_1")) + " " + tr("ERR_SER_UNRECOGNIZED_2") + " " + data.pop_front() as String)
		broken = true
		return
	else:
		data.pop_front()
	
	if not _is_next_type_correct(TYPE_STRING_ARRAY, data[0]):
		return
	
	if not _is_content_type_JSON(data.pop_front()):
		push_error("ServerResponse -> _init(), provided data is not JSON format")
		_display_error(tr("ERR_SER_UNEX_TYPE"))
		broken = true
		return
	
	var json_body: JSONParseResult = JSON.parse((data.pop_front() as PoolByteArray).get_string_from_utf8())
	
	if not json_body.error == OK:
		push_error("ServerResponse -> _init(), error while parsing JSON. Error: " + json_body.error_string)
		_display_error(tr("ERR_SER_PARSING"))
		broken = true
		return
	
	if not _is_next_type_correct(TYPE_DICTIONARY, json_body.result):
		return
	
	raw_body = json_body.result as Dictionary
	
	if raw_body.has("msg"):
		push_warning("ServerResponse -> _init(), server error: " + raw_body["msg"])
		_display_error(tr(_server_errors[raw_body["msg"]]) if _server_errors.has(raw_body["msg"]) else (tr("ERR_SER_UNRECOGNIZED_1") + ": " + raw_body["msg"]))
		broken = true
		return
	
	response_type = _decide_on_type()
	
	match response_type:
		TYPE.LOGIN:
			login_data = LoginData.new(raw_body)
		TYPE.SYNC:
			sync_data = SyncData.new(raw_body)
		TYPE.AVATAR:
			avatar_data = AvatarData.new(raw_body)
		_:
			push_error("ServerResponse -> _init(), unrecognized response")
			_display_error(tr("ERR_SER_PARSING"))
			broken = true


func _decide_on_type() -> int:
	if raw_body.has("token"):
		return TYPE.LOGIN
	
	if raw_body.has_all(["shared", "anthills"]):
		return TYPE.SYNC
	
	if raw_body.has_all(["ext", "avatar"]):
		return TYPE.AVATAR
	
	return TYPE.UNRECOGNIZED


func _is_next_type_correct(check_type: int, value) -> bool:
	var result: bool = true if typeof(value) == check_type else false
	
	if not result:
		broken = true
		push_error("ServerResponse -> _is_next_type_correct(), provided value and type does not match (" + check_type as String + " - " + typeof(value) as String + ")")
	
	return result


func _is_content_type_JSON(headers: PoolStringArray) -> bool:
	var header_name: String = "Content-Type: "
	var json_value: String = "application/json"
	
	for header in headers:
		header = header as String
		
		if header.begins_with(header_name):
			if header.substr(header_name.length()) == json_value:
				return true
			else:
				return false
	
	return false


func _check_result_code(result_code: int) -> bool:
	if not result_code == HTTPRequest.RESULT_SUCCESS:
		push_error("ServerResponse -> _check_result_code(), request failed, code: " + result_code as String)
		_display_error(tr("ERROR") + ": " + tr(_request_errors[result_code]))
		broken = true
		return false
	
	return true


func set_login_data(_value: LoginData) -> void:
	push_error("ServerResponse -> set_login_data(), you can't save to this field")


func set_sync_data(_value: SyncData) -> void:
	push_error("ServerResponse -> set_sync_data(), you can't save to this field")


func set_avatar_data(_value: AvatarData) -> void:
	push_error("ServerResponse -> set_avatar_data(), you can't save to this field")
